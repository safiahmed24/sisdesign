<?php include '../partials/header.php' ?>
			<div class="col-md-10" style="border: 1px solid #ddd; padding: 15px">
				<div class="col-md-8">
					<h4>Users</h4>
				</div>
				<div class="col-md-4">
					<select class="form-control">
						<option>Student</option>
						<option>Teacher</option>
						<option>Admin Staff</option>
					</select>
				</div>
				<div style="clear: both;"></div>
				<hr>
				<table class="table table-bordered table-stripped">
					<thead>
						<tr>
							<th>Sl No</th>
							<th>First Name</th>
							<th>Last Name</th>
							<th>Email</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody> 
						<tr> 
							<th scope="row">1</th> 
							<td>Mark</td> 
							<td>Otto</td> 
							<td>@mdo</td> 
							<td>
								<a href="users/create.php" class="btn btn-sm btn-success">Edit</a>&nbsp; &nbsp;
								<button class="btn btn-sm btn-danger">Delete</button>
							</td> 
						</tr> 
						<tr> 
							<th scope="row">2</th> 
							<td>Jacob</td>
							 <td>Thornton</td>
							 <td>@fat</td>
							<td>
								<a href="users/create.php" class="btn btn-sm btn-success">Edit</a>&nbsp; &nbsp;
								<button class="btn btn-sm btn-danger">Delete</button>
							</td> 
						</tr>
						<tr> 
						 	<th scope="row">3</th>
							 <td>Larry</td>
							 <td>the Bird</td>
							 <td>@twitter</td>
							<td>
								<a href="users/create.php" class="btn btn-sm btn-success">Edit</a>&nbsp; &nbsp;
								<button class="btn btn-sm btn-danger">Delete</button>
							</td> 
						</tr>
						<tr> 
							<th scope="row">4</th> 
							<td>Jacob</td>
							 <td>Thornton</td>
							 <td>@fat</td>
							<td>
								<a href="users/create.php" class="btn btn-sm btn-success">Edit</a>&nbsp; &nbsp;
								<button class="btn btn-sm btn-danger">Delete</button>
							</td> 
						</tr>
						<tr> 
						 	<th scope="row">5</th>
							 <td>Larry</td>
							 <td>the Bird</td>
							 <td>@twitter</td>
							<td>
								<a href="users/create.php" class="btn btn-sm btn-success">Edit</a>&nbsp; &nbsp;
								<button class="btn btn-sm btn-danger">Delete</button>
							</td> 
						</tr>
						<tr> 
							<th scope="row">6</th> 
							<td>Mark</td> 
							<td>Otto</td> 
							<td>@mdo</td> 
							<td>
								<a href="users/create.php" class="btn btn-sm btn-success">Edit</a>&nbsp; &nbsp;
								<button class="btn btn-sm btn-danger">Delete</button>
							</td> 
						</tr> 
						<tr> 
							<th scope="row">7</th> 
							<td>Jacob</td>
							 <td>Thornton</td>
							 <td>@fat</td>
							<td>
								<a href="users/create.php" class="btn btn-sm btn-success">Edit</a>&nbsp; &nbsp;
								<button class="btn btn-sm btn-danger">Delete</button>
							</td> 
						</tr>
						<tr> 
						 	<th scope="row">8</th>
							 <td>Larry</td>
							 <td>the Bird</td>
							 <td>@twitter</td>
							<td>
								<a href="users/create.php" class="btn btn-sm btn-success">Edit</a>&nbsp; &nbsp;
								<button class="btn btn-sm btn-danger">Delete</button>
							</td> 
						</tr>
						<tr> 
							<th scope="row">9</th> 
							<td>Jacob</td>
							 <td>Thornton</td>
							 <td>@fat</td>
							<td>
								<a href="users/create.php" class="btn btn-sm btn-success">Edit</a>&nbsp; &nbsp;
								<button class="btn btn-sm btn-danger">Delete</button>
							</td> 
						</tr>
						<tr> 
						 	<th scope="row">10</th>
							 <td>Larry</td>
							 <td>the Bird</td>
							 <td>@twitter</td>
							<td>
								<a href="users/create.php" class="btn btn-sm btn-success">Edit</a>&nbsp; &nbsp;
								<button class="btn btn-sm btn-danger">Delete</button>
							</td> 
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>
<?php include '../partials/footer.php' ?>
