<?php include '../partials/header.php' ?>
			<div class="col-md-10" style="border: 1px solid #ddd; padding: 15px">
				<h4>Create User</h4>
				<hr>
				<form>
					<div class="col-md-6">
					  <div class="form-group">
					    <label for="exampleInputEmail1">Email address</label>
					    <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Email">
					  </div>
					</div>
					<div class="col-md-6">
					  <div class="form-group">
					    <label for="exampleInputusername">User Name</label>
					    <input type="text" class="form-control" id="exampleInputusername" placeholder="User Name">
					  </div>
					</div>
					<div class="col-md-6">
					  <div class="form-group">
					    <label for="firstName">First Name</label>
					    <input type="text" class="form-control" id="firstName" placeholder="First Name">
					  </div>
					</div>
					<div class="col-md-6">
					  <div class="form-group">
					    <label for="middleName">Middle Name</label>
					    <input type="text" class="form-control" id="middleName" placeholder="Middle Name">
					  </div>
					</div>
					<div class="col-md-6">
					  <div class="form-group">
					    <label for="LastName">Last Name</label>
					    <input type="text" class="form-control" id="LastName" placeholder="Last Name">
					  </div>
					</div>
					<div class="col-md-6">
					  <div class="form-group">
					    <label for="exampleInputPassword1">Password</label>
					    <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
					  </div>
					</div>
					<div class="col-md-6">
					  <div class="form-group">
					    <label for="mobile">Mobile Number</label>
					    <input type="number" class="form-control" id="mobile" placeholder="Mobile Number">
					  </div>
					</div>
					<div class="col-md-6">
					  <div class="form-group">
					    <label for="phone">Phone Number</label>
					    <input type="number" class="form-control" id="phone" placeholder="Mobile Number">
					  </div>
					</div>
					<div class="col-md-6">
					  <div class="form-group">
					    <label for="exampleInputFile">Profile Pic</label>
					    <input type="file" id="exampleInputFile">
					    <p class="help-block">Upload your profile pic.</p>
					  </div>
					</div>
					<div class="col-md-6">
					  <div class="form-group">
					    <label for="role">Role</label>
					    <select class="form-control" id="role">
					    	<option>Student</option>
					    	<option>Teacher</option>
					    	<option>Admin</option>
					    </select>
					  </div>
					</div>
				  <button type="submit" class="btn btn-success">Submit</button>
				</form>
			</div>
		</div>
	</div>
<?php include '../partials/footer.php' ?>
